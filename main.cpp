#include "config.h"
#include <sstream>
#include <strstream>
#include <iostream>
#include <math.h>
#include "network.h"
#include "randomqueue.h"
#include "pipe.h"
#include "eventlist.h"
#include "logfile.h"
#include "loggers.h"
#include "clock.h"
#include "mtcp.h"
#include "exoqueue.h"

string ntoa(double n);
string itoa(uint64_t n);

// Simulation params
simtime_picosec RTT1=timeFromMs(0.3);
simtime_picosec RTT2=timeFromMs(250);
double targetwnd = 30;
int NUMFLOWS = 2;

#define TCP 10000

linkspeed_bps SERVICE = speedFromMbps(1.0);//NUMFLOWS * targetwnd/timeAsSec(RTT1));

#define RANDOM_BUFFER 3

#define FEEDER_BUFFER 100

mem_b BUFFER=memFromPkt(RANDOM_BUFFER+100);//NUMFLOWS * targetwnd);

void exit_error(char* progr){
  cout << "Usage " << progr << " [UNCOUPLED(DEFAULT)|COUPLED_INC|FULLY_COUPLED]" << endl;
  exit(1);
}

int main(int argc, char **argv) {
  EventList eventlist;
  eventlist.setEndtime(timeFromSec(3));
  Clock c(timeFromSec(50/100.), eventlist);

  /*
  if (argc>1){
    if (!strcmp(argv[1],"UNCOUPLED"))
      algo = UNCOUPLED;
    else if (!strcmp(argv[1],"COUPLED_INC"))
      algo = COUPLED_INC;
    else if (!strcmp(argv[1],"FULLY_COUPLED"))
      algo = FULLY_COUPLED;
    else if (!strcmp(argv[1],"COUPLED_TCP"))
      algo = COUPLED_TCP;
    else
      exit_error(argv[0]);
  } */

  srand(time(NULL));

  // prepare the loggers

  Queue* pqueue;

  TcpRtxTimerScanner tcpRtxScanner(timeFromMs(10), eventlist);

  //TCP flows on path 1
 Pipe pipe1(RTT1, eventlist); pipe1.setName("pipe1");

  TcpSrc* tcpSrc;
  TcpSink* tcpSnk;
  route_t* routeout;
  route_t* routein;
  double extrastarttime;

  for (int i=0;i<TCP;i++){
    tcpSrc = new TcpSrc(NULL,NULL,eventlist); tcpSrc->setName("Tcpflow0");
    tcpSnk = new TcpSink(); tcpSnk->setName("Tcpflow0");

    tcpRtxScanner.registerTcp(*tcpSrc);

	  // tell it the route
    pqueue = new Queue(SERVICE, memFromPkt(FEEDER_BUFFER), eventlist,NULL);

    routeout = new route_t(); routeout->push_back(pqueue);routeout->push_back(&pipe1); routeout->push_back(tcpSnk);

    routein  = new route_t();
    routein->push_back(tcpSrc);

    extrastarttime = drand()*50;
    tcpSrc->connect(*routeout,*routein,*tcpSnk,timeFromMs(extrastarttime), 1000000, "TCP"+itoa(i), 1000);
  }

//This Flow will not get completed
    tcpSrc = new TcpSrc(NULL,NULL,eventlist);
    tcpSnk = new TcpSink();

    tcpRtxScanner.registerTcp(*tcpSrc);

	  // tell it the route
    pqueue = new Queue(SERVICE, memFromPkt(FEEDER_BUFFER), eventlist,NULL);

    routeout = new route_t(); routeout->push_back(pqueue);routeout->push_back(&pipe1); routeout->push_back(tcpSnk);

    routein  = new route_t();
    routein->push_back(tcpSrc);

    extrastarttime = drand()*50;
    tcpSrc->connect(*routeout,*routein,*tcpSnk,timeFromMs(extrastarttime), 1000, "TCPMy flow", 1000);
//This Flow will not get completed

	// GO!
  while (eventlist.doNextEvent()) {}
}

string ntoa(double n) {
	stringstream s;
	s << n;
	return s.str();
	}
string itoa(uint64_t n) {
	stringstream s;
	s << n;
	return s.str();
	}
